import UIfx from 'uifx'; 

import snareSound from '../sounds/snare.mp3';
import hatSound from '../sounds/Hat.mp3';
import hat2Sound from '../sounds/Hat2.mp3';
import kickSound from '../sounds/Kick.mp3';
import kick2Sound from '../sounds/Kick2.mp3';
import crashSound from '../sounds/Crash.mp3';
import fxSound from '../sounds/fx.mp3';
import fx2Sound from '../sounds/FX2.mp3';
import tomSound from '../sounds/Tom.mp3';


const snare = new UIfx(snareSound);
const hat = new UIfx(hatSound);
const hat2 = new UIfx(hat2Sound);
const kick = new UIfx(kickSound);
const kick2 = new UIfx(kick2Sound);
const crash = new UIfx(crashSound);
const fx = new UIfx(fxSound);
const fx2 = new UIfx(fx2Sound);
const tom = new UIfx(tomSound);

export const bank_acoustic = [
    {name: "snare", sound: snare,},
    {name: "hat",   sound: hat,  },
    {name: "hat2",  sound: hat2, },
    {name: "kick",  sound: kick, },
    {name: "kick2", sound: kick2,},
    {name: "crash", sound: crash,},
    {name: "fx",    sound: fx,   },
    {name: "fx2",   sound: fx2,  },
    {name: "tom",   sound: tom,  }
];
export const bank_electro = [
    {name: "snare", sound: kick},
    {name: "hat",   sound: kick},
    {name: "hat2",  sound: kick},
    {name: "kick",  sound: kick},
    {name: "kick2", sound: kick},
    {name: "crash", sound: kick},
    {name: "fx",    sound: kick},
    {name: "fx2",   sound: kick},
    {name: "tom",   sound: kick}
];
export const bank_rock = [
    {name: "snare", sound: hat},
    {name: "hat",   sound: hat},
    {name: "hat2",  sound: hat},
    {name: "kick",  sound: hat},
    {name: "kick2", sound: hat},
    {name: "crash", sound: hat},
    {name: "fx",    sound: hat},
    {name: "fx2",   sound: hat},
    {name: "tom",   sound: hat}
];


