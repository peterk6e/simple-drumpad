import React, { Component } from "react";
import Range from './InputRange';

import './../App.css';




class Controls extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 'Acoustic'
        };
    }

getInitialState = () => {
    return {
        value: 'Acoustic'
    }
}

change = (event) => {
    this.setState({ value: event.target.value });
    this.props.setStyle(event.target.value);
}

render(){
    return (
        <div className="controls">

            <Range/>
                <select className = "preset" id="preset" onChange={this.change} value={this.state.value} >
                  <option value="Acoustic">Acoustic</option>
                  <option value="Electro">Electro</option>
                  <option value="Rock">Rock</option>
               </select>
        </div>           
    )
  }
}

export default Controls;