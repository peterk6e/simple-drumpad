import React, { Component } from "react";
import Pad from './Pad';
import './../App.css';


import {bank_acoustic} from './Banks';
import {bank_electro} from './Banks';
import {bank_rock} from './Banks';


class Box extends Component {
    constructor(props) {
        super(props);

        this.state = { 
            bank : bank_acoustic,
            style: "Acoustic" 
        };
    }


componentDidUpdate(prevProps) {
    if(prevProps.style !== this.props.style) {
        this.setState({style: this.props.style});
        switch (this.props.style) {
            case "Acoustic":
                this.setState({bank : bank_acoustic})
                break;
            case "Electro":
                this.setState({bank : bank_electro})
                break;
            case "Rock":
                this.setState({bank : bank_rock})
                break;
            default:
                break;
      }
    }
}


render(){

    return (
        <div className={`box ${this.props.style}`}>
            <Pad name="SN" sound={this.state.bank[0].sound}   eventKey='q'></Pad>
            <Pad name="HH" sound={this.state.bank[1].sound}   eventKey='w'></Pad>
            <Pad name="HH" sound={this.state.bank[2].sound}   eventKey='e'></Pad>
            <Pad name="KK" sound={this.state.bank[3].sound}   eventKey='a'></Pad>
            <Pad name="KK" sound={this.state.bank[4].sound}   eventKey='s'></Pad>
            <Pad name="CH" sound={this.state.bank[5].sound}   eventKey='d'></Pad>
            <Pad name="FX" sound={this.state.bank[6].sound}   eventKey='z'></Pad>
            <Pad name="FX" sound={this.state.bank[7].sound}   eventKey='x'></Pad>
            <Pad name="TM" sound={this.state.bank[8].sound}   eventKey='c'></Pad>
        </div>           
    )
  }
}

export default Box;