import React, { Component } from "react";
import Box from './Box';
import Controls from './Controls';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import 'bootstrap/dist/css/bootstrap.min.css';
import './../App.css';



class Drum extends Component {
    constructor(props) {
        super(props);
        this.state = {
            style:"Acoustic"
        };
    }

//callback function to get data from child : control
getStyle = (controlData) => {
    this.setState({style: controlData})
}
   



render() {
    return(
      <Container>
        <Row>
          <Col></Col>

          <Col >
            <Box style = {this.state.style} />
          </Col>

          <Col>
            <Controls setStyle = {this.getStyle}/>
          </Col>
          
          <Col></Col>
        </Row>
      </Container>
    )
}

}

export default Drum;