import React, { Component } from 'react';

export default class InputRange extends Component {
  state = {
    value: 0,
  }

  onChange = (event) => {
    this.setState({ value: event.target.value });
  }

  render() {
    return (
      <div>
        <div>volume : {this.state.value}</div>
        <input
          type="range"
          value={this.state.value}
          onChange={this.onChange}
        />
      </div>
    )
  }
}