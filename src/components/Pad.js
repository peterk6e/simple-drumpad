import React, { Component } from "react";
import './../App.css';
import KeyboardEventHandler from 'react-keyboard-event-handler';




class Pad extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }


   
 playSound = () => {
        this.props.sound.play()
        console.log(this.props.name)
     }
 

 handleKeyPress = (event) => {
    // if(event.key === this.prop.trig){
    //   console.log('space press here! ')
    // }
  }




render() {
    return(
        <button className={`pad  ${this.props.name}`} onClick={this.playSound} >
            <p>{this.props.name}</p>
            <i>{this.props.eventKey}</i>
            <KeyboardEventHandler
                handleKeys={['a','q','w','e','s','d','z','x','c']}
                onKeyEvent={(key, e) => this.props.eventKey === key && this.playSound()} />
        </button>

    )
}

}

export default Pad;