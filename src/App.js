import React from 'react';
import Drum from './components/Drum';
import './App.css';



function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Drum/>
      </header>
    </div>
  );
}

export default App;



